---
title: Home
layout: default
---

# L'association

Favilla est une association loi 1901 dédiée à la promotion et au soutien de la
création de spectacles dans l'espace public. Elle intervient auprès des
compagnies qu'elle soutient, par tous les moyens utiles, que ce soit en tant
que co-producteur, qu'organisateur d'évènements, ou que plateforme logistique.
